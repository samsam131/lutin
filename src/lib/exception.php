<?php

namespace Application\Lib\Exception;
use Exception;
use RuntimeException;
use LogicException;

class CommissionIdentifierException extends RuntimeException
{
   public $message = 'Aucun identifiant de commission n\'a été trouvé';
}

class MemberIdentifierException extends RuntimeException
{
   public $message = 'Aucun identifiant de membre n\'a été trouvé';
}

class MeetingIdentifierException extends RuntimeException
{
   public $message = 'Aucun identifiant de réunion n\'a été trouvé';
}