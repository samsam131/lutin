<?php
//session_start();
//require_once('src/lib/database/database.php');
namespace Application\Model\User;

use Application\Lib\Database\DatabaseConnection;
use Exception;

class User
{
    public int $user_id;
    public string $first_name;
    public string $last_name;
    public string $email;
    public string $hash_password;
    public string $profile;
}

class UserRepository
{
    public DatabaseConnection $connection;

    /*public function usersDbConnect()
    {
        if ($this->database === null) {
            try {
                $this->database = new PDO('mysql:host=localhost;dbname=lutin;charset=utf8', 'root', 'root');
            } catch (Exception $e) {
                die('Erreur : ' . $e->getMessage());
            }
        }
    }*/
    public function getUsers(): array
    {
        //$this->connection->getConnection(); // We connect to the database.

        // We retrieve all commissions.
        $statement = $this->connection->getConnection()->query(
            "SELECT * FROM users ORDER BY last_name"
        );
        $users = [];
        while (($row = $statement->fetch())) {
            $user = new User();

            $user->user_id = $row['user_id'];
            $user->first_name = $row['first_name'];
            $user->last_name = $row['last_name'];
            $user->email = $row['email'];
            $user->hash_password = $row['password'];
            $user->profile = $row['profile'];

            $users[] = $user;
        }
        return $users;
    }

    public function createUser($first_name, $last_name, $email, $password, $profile)
    {
        //$this->usersDbConnect();

        $sqlQuery = ('INSERT INTO users (first_name, last_name, email, password, profile)
    VALUES(?, ?, ?, ?, ?)');

        $statement = $this->connection->getConnection()->prepare($sqlQuery);
        $hash_password = password_hash($password, PASSWORD_BCRYPT);
        $affectedLines = $statement->execute([$first_name, $last_name, $email, $hash_password, $profile]);

        return ($affectedLines > 0);
    }
    
    public function loginCheck($email, $password)
    {
        //$this->usersDbConnect();
        $sqlQuery = ("SELECT * FROM users WHERE email = ?");
        $statement = $this->connection->getConnection()->prepare($sqlQuery);
        $statement->execute([$email]);
        $user = [];
        $row2 = $statement->rowCount();
        $row = $statement->fetch();

        if ($row2 > 0) {
            $user = new User();
            $user->user_id = $row['user_id'];
            $user->first_name = $row['first_name'];
            $user->last_name = $row['last_name'];
            $user->email = $row['email'];
            //$user->password = $row['password'];
            $user->hash_password = $row['password'];
            $user->profile = $row['profile'];

            //if($password === $user->password){
            if (password_verify($password, $user->hash_password)) {
                $_SESSION['email'] = $user->email;
                $_SESSION['first_name'] = $user->first_name;
                $_SESSION['last_name'] = $user->last_name;
                $_SESSION['profile'] = $user->profile;
                header('Location: index.php?action=home');
                
                die();
            } else {
                throw new Exception ('mot de passe incorrect');
            }
        } else {
            throw new Exception ('utilisateur inconnu');
        }
    }
}
