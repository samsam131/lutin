<?php

namespace Application\Model\Meeting;

use Application\Lib\Database\DatabaseConnection;

class Meeting
{
    public int $meeting_id;
    public int $commission_id;
    public $meeting_date;
    public bool $quorum;
    public $agenda;
    public $url_minutes;
    public $ppd;
    public $rpd;
    public $invitation_date;
    public $status_minutes;
    public $assistant;
    public $president;
}
class MeetingRepository
{
    public DatabaseConnection $connection;

    public function getMeeting($meeting_id)
    {
        //$this->meetingsDbConnect(); // We connect to the database.

        // We retrieve one commission.
        $statement = $this->connection->getConnection()->prepare(
            "SELECT meeting_id, commission_id,
            date AS meeting_date_fr,/*DATE_FORMAT(date, '%d/%m/%Y') AS meeting_date_fr,*/
            quorum, agenda, url_minutes,
            /*DATE_FORMAT(provisional_preparation_date, '%d/%m/%Y') AS ppd_fr,
            DATE_FORMAT(real_preparation_date, '%d/%m/%Y') AS rpd_fr ,
            DATE_FORMAT(invitation_date, '%d/%m/%Y') AS invitation_date_fr,*/
            provisional_preparation_date AS ppd_fr, real_preparation_date AS rpd_fr, invitation_date AS invitation_date_fr,
            status_minutes, assistant, president
            FROM meeting WHERE meeting_id = ?"
        );
        $statement->execute([$meeting_id]);

        $row = $statement->fetch();
        $meeting = new Meeting();
        $meeting->meeting_id = $row['meeting_id'];
        $meeting->commission_id = $row['commission_id'];
        $meeting->meeting_date = $row['meeting_date_fr'];
        $meeting->quorum = $row['quorum'];
        $meeting->agenda = $row['agenda'];
        $meeting->url_minutes = $row['url_minutes'];
        $meeting->ppd = $row['ppd_fr'];
        $meeting->rpd = $row['rpd_fr'];
        $meeting->invitation_date = $row['invitation_date_fr'];
        $meeting->status_minutes = $row['status_minutes'];
        $meeting->assistant = $row['assistant'];
        $meeting->president = $row['president'];

        return $meeting;
    }
    public function createMeeting($commission_id, $date, bool $quorum, string $agenda, string $url_minutes, $provisional_preparation_date, $real_preparation_date, $invitation_date, string $status_minutes, string $assistant, string $president): bool
    {
        //$this->meetingsDbConnect(); // We connect to the database.

        /*
        // Variables déclaration
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $section = $_POST['section'];
        $organisation = $_POST['organisation'];
        $union_pro = $_POST['union_pro'];
        $url_img = $_POST['url_img'];
        
        //chek data
        if (
            !isset($first_name) || !isset($last_name) || !isset($email) || !isset($phone) || !isset($section) || !isset($organisation)
            ) {
                echo 'Merci de compléter les champs obligatoire';
        return;
            }
        */
        $sqlQuery = ('INSERT INTO meeting (commission_id, date, quorum, agenda, url_minutes, provisional_preparation_date, real_preparation_date, invitation_date, status_minutes, assistant, president )
                    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

        $statement = $this->connection->getConnection()->prepare($sqlQuery);

        $affectedLines = $statement->execute([$commission_id, $date, $quorum, $agenda, $url_minutes, $provisional_preparation_date, $real_preparation_date, $invitation_date, $status_minutes, $assistant, $president]);

        return ($affectedLines > 0);
    }

    public function updateMeetings(
        $meeting_id,
        //$commission_id,
        $meting_date,
        $quorum,
        $agenda,
        $url_minutes,
        $provisional_preparation_date,
        $real_preparation_date,
        $invitation_date,
        $status_minutes,
        $assistant,
        $president
    ): bool {

        //$this->meetingsDbConnect(); // We connect to the database.
        $query = ('UPDATE meeting SET date = ?, quorum = ?, agenda = ?, url_minutes = ?, provisional_preparation_date = ?, real_preparation_date = ?,
    invitation_date = ?, status_minutes = ?, assistant = ?, president = ? WHERE meeting_id = ?');
        $statement = $this->connection->getConnection()->prepare($query);

        $affectedLines = $statement->execute([
            $meting_date, $quorum, $agenda, $url_minutes, $provisional_preparation_date,
            $real_preparation_date, $invitation_date, $status_minutes, $assistant, $president, $meeting_id
        ]);

        return ($affectedLines > 0);
    }

    public function getMeetings($commission_id)
    {
        //$this->meetingsDbConnect();// We connect to the database.

        // We retrieve all meetings of one commission.
        $statement = $this->connection->getConnection()->prepare(
            "SELECT meeting_id, commission_id, DATE_FORMAT(date, '%d/%m/%Y') AS date_meeting_fr , quorum, agenda, url_minutes,
            DATE_FORMAT(provisional_preparation_date, '%d/%m/%Y') AS provisional_preparation_date_fr ,
            DATE_FORMAT(real_preparation_date, '%d/%m/%Y') AS real_preparation_date_fr ,
            DATE_FORMAT(invitation_date, '%d/%m/%Y') AS invitation_date_fr, status_minutes, assistant, president
        FROM meeting WHERE commission_id = ? ORDER BY date_meeting_fr"
        );
        $statement->execute([$commission_id]);
        $meetings = [];
        while (($row = $statement->fetch())) {
            $meeting = new Meeting();
            $meeting->meeting_id = $row['meeting_id'];
            $meeting->commission_id = $row['commission_id'];
            $meeting->meeting_date = $row['date_meeting_fr']; //idem
            $meeting->quorum = $row['quorum'];
            $meeting->agenda = $row['agenda'];
            $meeting->url_minutes = $row['url_minutes'];
            $meeting->ppd = $row['provisional_preparation_date_fr']; //Faire changement dans le template
            $meeting->rpd = $row['real_preparation_date_fr']; //idem
            $meeting->invitation_date = $row['invitation_date_fr'];
            $meeting->status_minutes = $row['status_minutes'];
            $meeting->assistant = $row['assistant'];
            $meeting->president = $row['president'];

            $meetings[] = $meeting;
        }
        return $meetings;
    }
    /*public function meetingsDbConnect()
{
    if ($this->database === null) {
        try {
            $this->database = new PDO('mysql:host=localhost;dbname=lutin;charset=utf8', 'root', 'root');
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
}*/
}

