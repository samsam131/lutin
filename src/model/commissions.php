<?php

namespace Application\Model\Commission;

use Application\Lib\Database\DatabaseConnection;

class Commission
{
    public int $commission_id;
    public string $name;
    public string $nickname;
    public string $installation_date;
    public string $frequency;
    public $president;
    public $vice_president;
    public $img_url;
    public $url_agreement;
    public $url_invitation_template;
    public $url_minutes_template;
    public $alternation_rules;
    public $composition_social_section;
    public $composition_professionnal_section;
}

interface DatabaseInterface {

    public function execute($query) : self;
    
    public function fetchAll() : array;
}

class GetAllCommissionsRepository
{
    public $connection;

    public function getCommissions(/*CommissionRepository $this*/)
    {
        //$this->commissionsDbConnect(); // We connect to the database.

        // We retrieve all commissions.
        $statement = $this->connection->getConnection()->query(
            "SELECT commission_id, name, nickname, DATE_FORMAT(installation_date, '%d/%m/%Y') AS installation_date_fr, president, 'vice_president', frequency, url_img FROM commissions ORDER BY installation_date"
        );
        $commissions = [];
        while (($row = $statement->fetch())) {
            $commission = new Commission();
            $commission->commission_id = $row['commission_id'];
            $commission->name = $row['name'];
            $commission->nickname = $row['nickname'];
            $commission->installation_date = $row['installation_date_fr'];
            $commission->president = $row['president'];
            $commission->vice_president = $row['vice_president'];
            $commission->frequency = $row['frequency'];
            $commission->img_url = $row['url_img'];

            $commissions[] = $commission;
        }
        return $commissions;
    }
}


class GetOneCommissionRepository
{
    public DatabaseConnection $connection;
    /*
    public function getCommissions(CommissionRepository $this)
    {
        //$this->commissionsDbConnect(); // We connect to the database.

        // We retrieve all commissions.
        $statement = $this->connection->getConnection()->query(
            "SELECT commission_id, name, nickname, DATE_FORMAT(installation_date, '%d/%m/%Y') AS installation_date_fr, president, 'vice_president', frequency, url_img FROM commissions ORDER BY installation_date"
        );
        $commissions = [];
        while (($row = $statement->fetch())) {
            $commission = new Commission();
            $commission->commission_id = $row['commission_id'];
            $commission->name = $row['name'];
            $commission->nickname = $row['nickname'];
            $commission->installation_date = $row['installation_date_fr'];
            $commission->president = $row['president'];
            $commission->vice_president = $row['vice_president'];
            $commission->frequency = $row['frequency'];
            $commission->img_url = $row['url_img'];

            $commissions[] = $commission;
        }
        return $commissions;
    }
    */
    public function getCommission(/*CommissionRepository $repository,*/$commission_id)
    {
        // We connect to the database.
        //$this->commissionsDbConnect();

        // We retrieve one commission.
        $statement = $this->connection->getConnection()->prepare(
            "SELECT commission_id, name, nickname, DATE_FORMAT(installation_date, '%d/%m/%Y') AS installation_date_fr, president, `vice_president`, frequency,
        url_img, url_agreement, url_invitation_template, url_minutes_template,
        alternation_rules, composition_social_section, composition_professionnal_section
        FROM commissions WHERE commission_id = ?"
        );
        $statement->execute([$commission_id]);

        $row = $statement->fetch();
        $commission = new Commission();
        $commission->commission_id = $row['commission_id'];
        $commission->name = $row['name'];
        $commission->nickname = $row['nickname'];
        $commission->installation_date = $row['installation_date_fr'];
        $commission->president = $row['president'];
        $commission->vice_president = $row['vice_president'];
        $commission->frequency = $row['frequency'];
        $commission->img_url = $row['url_img'];
        $commission->url_agreement = $row['url_agreement'];
        $commission->url_invitation_template = $row['url_invitation_template'];
        $commission->url_minutes_template = $row['url_minutes_template'];
        $commission->alternation_rules = $row['alternation_rules'];
        $commission->composition_social_section = $row['composition_social_section'];
        $commission->composition_professionnal_section = $row['composition_professionnal_section'];

        return $commission;
    }
    //connection to database
    /*public function commissionsDbConnect()
    {
        if ($this->database === null) {
            try {
                $this->database = new PDO('mysql:host=localhost;dbname=lutin;charset=utf8', 'root', 'root');
            } catch (Exception $e) {
                die('Erreur : ' . $e->getMessage());
            }
        }
    }*/
}
