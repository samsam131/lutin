<?php
namespace Application\Model\Member;

use Application\Lib\Database\DatabaseConnection;

class Member
{
    public int $member_id;
    public int $commission_id;
    public string $first_name;
    public string $last_name;
    public string $email;
    public $phone = null;
    public string $section;
    public string $organisation;
    public $union_pro;
    public $url_img;
}

class MemberRepository
{
    public DatabaseConnection $connection;

    /*public function memberDbConnect()
    {
        if ($this->database === null) {
            try {
                $this->database = new PDO('mysql:host=localhost;dbname=lutin;charset=utf8', 'root', 'root');
            } catch (Exception $e) {
                die('Erreur : ' . $e->getMessage());
            }
        }
    }*/
    public function createMember(string $first_name, string $last_name, string $email, string $phone, string $section, string $organisation, string $union_pro, string $url_img): bool
    {
        //$this->memberDbConnect(); // We connect to the database.
        $sqlQuery = ('INSERT INTO members (first_name, last_name, email, phone, section, organisation, union_pro, url_img )
                    VALUES(?, ?, ?, ?, ?, ?, ?, ?)');
        $statement = $this->connection->getConnection()->prepare($sqlQuery);
        $affectedLines = $statement->execute([$first_name, $last_name, $email, $phone, $section, $organisation, $union_pro, $url_img]);
        return ($affectedLines > 0);
    }

    public function commissionsMember($member_id, $commissions)
    {
        //$this->memberDbConnect();
        $sqlQuery = ('INSERT INTO members_commissions (member_id, commission_id) VALUES (?, ?)');
        $statement = $this->connection->getConnection()->prepare($sqlQuery);
        foreach ($commissions as $value) {
            $affectedLines = $statement->execute([$member_id, $value]);
            return ($affectedLines > 0);
        }
    }

    function getMembers()
    {
        //$this->memberDbConnect(); // We connect to the database.

        // We retrieve all members for all commissions.
        $statement = $this->connection->getConnection()->query("SELECT * FROM members ORDER BY section, organisation, last_name");
        $members = [];
        while ($row = $statement->fetch()) {
            $member = new Member();
            $member->member_id = $row['member_id'];
            $member->first_name = $row['first_name'];
            $member->last_name = $row['last_name'];
            $member->email = $row['email'];
            $member->phone = $row['phone'];
            $member->section = $row['section'];
            $member->union_pro = $row['union_pro'];
            $member->organisation = $row['organisation'];
            $member->url_img = $row['url_img'];

            $members[] = $member;
        }
        return $members;
    }

    public function getMember($member_id)
    {
        //$this->memberDbConnect(); // We connect to the database.
        // We retrieve data of the members.
        $statement = $this->connection->getConnection()->prepare("SELECT * FROM members WHERE member_id = ?");
        $statement->execute([$member_id]);
        $row = $statement->fetch();
        $member = new Member();
        $member->member_id = $row['member_id'];
        $member->first_name = $row['first_name'];
        $member->last_name = $row['last_name'];
        $member->email = $row['email'];
        $member->phone = $row['phone'];
        $member->section = $row['section'];
        $member->union_pro = $row['union_pro'];
        $member->organisation = $row['organisation'];
        $member->url_img = $row['url_img'];

        return $member;
    }

    public function updateMembers($member_id, string $first_name, string $last_name, string $email, string $phone, string $section, string $organisation, string $union_pro, string $url_img): bool
    {
        //$this->memberDbConnect(); // We connect to the database.
        $statement = $this->connection->getConnection()->prepare("UPDATE members SET first_name = ?, last_name = ?, email = ?, phone = ?, section = ?, organisation = ?, union_pro = ?, url_img = ? WHERE member_id = ?");
        $affectedLines = $statement->execute([$first_name, $last_name, $email, $phone, $section, $organisation, $union_pro, $url_img, $member_id]);

        return ($affectedLines > 0);
    }



    public function getMembersCommission($commission_id)
    {
        //$this->memberDbConnect(); // We connect to the database.

        // We retrieve all members for one commission).
        $query =
            "SELECT
        m.first_name as first_name,
        m.last_name as last_name,
        m.email as email,
        m.section AS section,
        m.organisation AS organisation,
        m.union_pro AS union_pro,
        m.phone AS phone,
        mc.member_id as member_id,
        mc.commission_id as commission_id,
        c.nickname
    FROM members_commissions mc
    JOIN members m ON m.member_id = mc.member_id
    JOIN commissions c ON mc.commission_id = c.commission_id
    WHERE mc.commission_id = ?
    ORDER BY section, organisation, union_pro";

        $statement = $this->connection->getConnection()->prepare($query);
        $statement->execute([$commission_id]);
        $commission_members = [];

        while ($row = $statement->fetch()) {
            $commission_member = new Member();
            $commission_member->member_id = $row['member_id'];
            $commission_member->commission_id = $row['commission_id'];
            $commission_member->first_name = $row['first_name'];
            $commission_member->last_name = $row['last_name'];
            $commission_member->email = $row['email'];
            $commission_member->phone = $row['phone'];
            $commission_member->section = $row['section'];
            $commission_member->union_pro = $row['union_pro'];
            $commission_member->organisation = $row['organisation'];

            $commission_members[] = $commission_member;
        }
        return $commission_members;
    }
}
