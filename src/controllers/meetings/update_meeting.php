<?php

namespace Application\Controllers\UpdateMeeting;

require_once('src/model/meeting.php');
require_once('src/lib/database/database.php');

use Application\Model\Meeting\MeetingRepository;
use Application\Lib\Database\DatabaseConnection;

class UpdateMeeting
{
   public function execute($meeting_id, ?array $input)
   {
      $meetingsRepository = new MeetingRepository();
      $meetingsRepository->connection = new DatabaseConnection();

      // It handles the form submission when there is an input.
      if ($input !== null) {
         //$commission_id = $_GET['id'];
         $meeting_date = null;
         $quorum = null;
         $agenda = null;
         $url_minutes = null;
         //$provisional_preparation_date = null;
         //$real_preparation_date = null;
         //$invitation_date = null;
         $status_minutes = null;
         $assistant = null;
         $president = null;

         if (
            !empty($input['meeting_date']) && !empty($input['president']) && !empty($input['status_minutes']) && !empty($input['assistant']) && !empty($input['president'])
            && $input['meeting_date'] > date('m/d/Y')  && $input['real_preparation_date'] < $input['meeting_date'] && $input['invitation_date'] < $input['meeting_date']
         ) {
            //$commission_id = $input['commission_id'];
            $meeting_date = $input['meeting_date'];
            $quorum = $input['quorum'];
            $agenda = $input['agenda'];
            $url_minutes = $input['url_minutes'];
            $provisional_preparation_date = $input['provisional_preparation_date'];
            $real_preparation_date = $input['real_preparation_date'];
            $invitation_date = $input['invitation_date'];
            $status_minutes = $input['status_minutes'];
            $assistant = $input['assistant'];
            $president = $input['president'];
         } else {
            throw new \Exception ('Les données du formulaire sont invalides.');
         }

         //Need to add other controls ?

         $success = $meetingsRepository->updateMeetings(
            $meeting_id,
            $meeting_date,
            $quorum,
            $agenda,
            $url_minutes,
            $provisional_preparation_date,
            $real_preparation_date,
            $invitation_date,
            $status_minutes,
            $assistant,
            $president
         );
         if (!$success) {
            throw new \Exception('Impossible de modifier le membre !');
         } else {
            header('Location : index.php?action=home');
         }

         //require('template/update_member.php');
      } //else {
      // Otherwise, it displays the form.
      $meeting = $meetingsRepository->getMeeting($meeting_id);
      if ($meeting === null) {
         throw new \Exception ("la réunion n'existe pas");
      }
      //}

      require('templates/meetings/update_meeting.php');
   }
}
