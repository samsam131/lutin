<?php

namespace Application\Controllers\AddMeeting;

require_once('src/model/meeting.php');
require_once('src/lib/database/database.php');

use Application\Model\Meeting\MeetingRepository;
use Application\Lib\Database\DatabaseConnection;

class AddmeetingForm
{
    public function execute($commision_id)
    {
        $commission_id = $_GET['id'];
        require('templates/meetings/add_meeting.php');
    }
}

class AddMeeting
{
    public function execute($commision_id, array $input)
    {
        $meetingsRepository = new MeetingRepository();
        $meetingsRepository->connection = new DatabaseConnection();

        $commission_id = $_GET['id'];
        $date = null;
        $quorum = true;
        $agenda = null;
        $url_minutes = null;
        $provisional_preparation_date = null;
        $real_preparation_date = null;
        $invitation_date = null;
        $status_minutes = null;
        $assistant = null;
        $president = null;

        if (
            !empty($input['meeting_date']) && !empty($input['president']) && !empty($input['status_minutes']) && !empty($input['assistant']) && !empty($input['president'])
            && $input['meeting_date'] > date('m/d/Y')  && $input['real_preparation_date'] < $input['meeting_date'] && $input['invitation_date'] < $input['meeting_date']
        ) {
            //vérifier la conformité des dates ?

            $date = $input['meeting_date'];
            $quorum = $input['quorum'];
            $agenda = $input['agenda'];
            $url_minutes = $input['url_minutes'];
            $provisional_preparation_date = $input['provisional_preparation_date'];
            $real_preparation_date = $input['real_preparation_date'];
            $invitation_date = $input['invitation_date'];
            $status_minutes = $input['status_minutes'];
            $assistant = $input['assistant'];
            $president = $input['president'];
        } else {
            throw new \Exception('Les données du formulaire sont invalides.');
        }
        //Need to add other controls ?

        $success = $meetingsRepository->createMeeting($commission_id, $date, $quorum, $agenda, $url_minutes, $provisional_preparation_date, $real_preparation_date, $invitation_date, $status_minutes, $assistant, $president);
        if (!$success) {
            throw new \Exception('Impossible de créer la réunion !');
        } else {
            header('Location : index.php?action=commission&id=' . $commision_id); // Don't working
        }
    }
}
