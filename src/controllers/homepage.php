<?php

namespace Application\Controllers\Homepage;

require_once('src/model/commissions.php');
require_once('src/lib/database/database.php');

//use Application\Model\Commission\CommissionRepository;
use Application\Lib\Database\DatabaseConnection;
use Application\Model\Commission\GetAllCommissionsRepository;

class Homepage {
	public function execute() {
		$commissionsRepository = new GetAllCommissionsRepository();
		$commissionsRepository->connection = new DatabaseConnection();
		$commissions= $commissionsRepository->getCommissions();
	
		require('templates/homepage.php');
	}
}
