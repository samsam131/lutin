<?php

namespace Application\Controllers\AddMember;

require_once('src/model/members.php');
require_once('src/lib/database/database.php');

use Application\Model\Member\MemberRepository;
use Application\Lib\Database\DatabaseConnection;


class AddMembersForm
{
   public function execute()
   {
      require('templates/members/add_members.php');
   }
}

class Addmembers
{
   public function execute(array $input)
   {
      $memberRepository = new MemberRepository();
      $memberRepository->connection = new DatabaseConnection();

      $first_name = null;
      $last_name = null;
      $email = null;
      $phone = null;
      $section = null;
      $organisation = null;
      $union_pro = null;
      $url_img = null;

      if (
         !empty($input['first_name']) && !empty($input['last_name']) && !empty($input['email']) && !empty($input['section']) && !empty($input['organisation'])
         && ctype_alpha($input['first_name']) && ctype_alpha($input['last_name']) && filter_var($input['email'], FILTER_VALIDATE_EMAIL)
         && strlen($input['phone']) === 10 && is_numeric($input['phone']) && ctype_alpha($input['section']) && ctype_alpha($input['organisation'])
         && ctype_alnum($input['union_pro'])
      ) {
         $first_name = $input['first_name'];
         $last_name = $input['last_name'];
         $email = $input['email'];
         $phone = $input['phone'];
         $section = $input['section'];
         $organisation = $input['organisation'];
         $union_pro = $input['union_pro'];
         $url_img = $input['url_img'];
      } else {
         throw new \Exception('Les données du formulaire sont invalides.');
      }
      //Need to add other controls

      $success = $memberRepository->createMember($first_name, $last_name, $email, $phone, $section, $organisation, $union_pro, $url_img);
      if (!$success) {
         throw new \Exception('Impossible d\'ajouter le membre !');
      } else {
         header('Location : http://localhost:8888/lutin/index.php?action=members');
      }
   }
}

class AddCommissionsMember
{
   public function execute($member_id, ?array $input)
   {
      $memberRepository = new MemberRepository();
      $memberRepository->connection = new DatabaseConnection();

      if ($input !== null) {
         $commissions = [];
         $commissions = $input;
         var_dump($commissions);
         echo $member_id;
         /*
      $input = [];
      if (isset($input['CPL_medecin'])){array_push($input, $input['CPL_medecin']);}
      if (isset($input['CPL_infirmier'])){array_push($input, $input['CPL_infirmier']);}
      */
         $success = $memberRepository->commissionsMember($member_id, $commissions);
         if (!$success) {
            throw new \Exception('Impossible d\'ajouter des commissions !');
         } else {
            header('Location : http://localhost:8888/lutin/index.php?action=members');
         }
      }

      require('templates/commissions/add_commissions_member.php');
   }
}
