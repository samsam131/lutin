<?php

namespace Application\Controllers\UpdateMember;

require_once('src/model/members.php');
require_once('src/lib/database/database.php');

use Application\Model\Member\MemberRepository;
use Application\Lib\Database\DatabaseConnection;

class UpdateMember
{
   public function execute($member_id, ?array $input)
   {
      $memberRepository = new MemberRepository();
      $memberRepository->connection = new DatabaseConnection();

      // It handles the form submission when there is an input.
      if ($input !== null) {
         $first_name = null;
         $last_name = null;
         $email = null;
         $phone = null;
         $section = null;
         $organisation = null;
         $union_pro = null;
         $url_img = null;

         if (
            !empty($input['first_name']) && !empty($input['last_name']) && !empty($input['email']) && !empty($input['section']) && !empty($input['organisation'])
            && ctype_alpha($input['first_name']) && ctype_alpha($input['last_name']) && filter_var($input['email'], FILTER_VALIDATE_EMAIL)
            && strlen($input['phone']) === 10 && is_numeric($input['phone']) && ctype_alpha($input['section']) && ctype_alpha($input['organisation'])
            && ctype_alnum($input['union_pro'])
         ) {
            $first_name = $input['first_name'];
            $last_name = $input['last_name'];
            $email = $input['email'];
            $phone = $input['phone'];
            $section = $input['section'];
            $organisation = $input['organisation'];
            $url_img = $input['url_img'];
            $union_pro = $input['union_pro'];
         } else {
            throw new \Exception ('Les données du formulaire sont invalides.');
         }

         $success = $memberRepository->updateMembers($member_id, $first_name, $last_name, $email, $phone, $section, $organisation, $union_pro, $url_img);
         if (!$success) {
            throw new \Exception('Impossible de modifier le membre !');
         } else {
            //header('Location : index.php?action=members');
         }

         //require('template/update_member.php');
      } //else {
      // Otherwise, it displays the form.
      $member = $memberRepository->getMember($member_id);
      if ($member === null) {
         throw new \Exception ("le membre $member_id identifier n'existe pas");
      }
      //}

      require('templates/members/update_member.php');
   }
}
