<?php

namespace Application\Controllers\Member;

require_once('src/model/members.php');
require_once('src/lib/database/database.php');

use Application\Model\Member\MemberRepository;
use Application\Lib\Database\DatabaseConnection;

class Members
{
	public function execute()
	{
		$memberRepository = new MemberRepository();
		$memberRepository->connection = new DatabaseConnection();
		$members = $memberRepository->getMembers();

		require('templates/members/members.php');
	}
}
