<?php

namespace Application\Controllers\MemberCommission;

require_once('src/model/members.php');
require_once('src/lib/database/database.php');

use Application\Model\Member\MemberRepository;
use Application\Lib\Database\DatabaseConnection;

class MembersCommission
{
  public function execute(string $commission_id)
  {
    $memberRepository = new MemberRepository();
    $memberRepository->connection = new DatabaseConnection();
    $commission_members = $memberRepository->getMembersCommission($commission_id);


    require('templates/members/members_commission.php');
  }
}
