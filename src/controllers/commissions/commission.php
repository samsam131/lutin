<?php

namespace Application\Controllers\Commission;

require_once('src/model/commissions.php');
require_once('src/model/meeting.php');
require_once('src/lib/database/database.php');


use Application\Model\Commission\GetOneCommissionRepository;
use Application\Model\Meeting\MeetingRepository;
use Application\Lib\Database\DatabaseConnection;


class Commissions
{
  public function execute(string $commission_id)
  {
    $commissionsRepository = new GetOneCommissionRepository();
    $commissionsRepository->connection = new DatabaseConnection();
    $commission = $commissionsRepository->getCommission($commission_id);

    $meetingsRepository = new MeetingRepository();
    $meetingsRepository->connection = new DatabaseConnection();
    $meetings = $meetingsRepository->getMeetings($commission_id);

    require('templates/commissions/commissions.php');
  }
}
