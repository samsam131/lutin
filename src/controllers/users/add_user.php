<?php

namespace Application\Controllers\AddUser;

require_once('src/model/users.php');

use Application\Model\User\UserRepository;
use Application\Lib\Database\DatabaseConnection;

class AddUser
{
    public function execute(?array $input)
    {
        $userRepository = new UserRepository();
        $userRepository->connection = new DatabaseConnection;

        if ($input !== null) {
            $first_name = null;
            $last_name = null;
            $email = null;
            $password = null;
            $profile = null;

            if (
                !empty($input['first_name']) && !empty($input['last_name']) && !empty($input['email']) && !empty($input['password']) && !empty($input['profile'])
                && ctype_alpha($input['first_name']) && ctype_alpha($input['last_name']) && filter_var($input['email'], FILTER_VALIDATE_EMAIL)
                && strlen($input['password']) === 8 && ctype_alpha($input['profile'])
            ) {
                $first_name = $input['first_name'];
                $last_name = $input['last_name'];
                $email = $input['email'];
                $password = $input['password'];
                $profile = $input['profile'];
            } else {
                throw new \Exception('Les données du formulaire sont invalides.');
            }
            //Need to add other controls

            $success = $userRepository->createUser($first_name, $last_name, $email, $password, $profile);
            if (!$success) {
                throw new \Exception('Impossible d\'ajouter l\'utilisateur !');
            } else {
                header('Location : http://localhost:8888/lutin/index.php?action=users');
            }
        }
        require('templates/users/add_users.php');
    }
}
