<?php

namespace Application\Controllers\User;

require_once('src/model/users.php');
require_once('src/lib/database/database.php');

use Application\Model\User\UserRepository;
use Application\Lib\Database\DatabaseConnection;

class Users
{
  public function execute()
  {
    $userRepository = new UserRepository();
    $userRepository->connection = new DatabaseConnection;
    $users = $userRepository->getUsers();

    require('templates/users/users.php');
  }
}
