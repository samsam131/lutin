<?php

namespace Application\Controllers\Login;

session_start();

require_once('src/model/users.php');

use Application\Model\User\UserRepository;
use Application\Lib\Database\DatabaseConnection;


class Login {
	public function execute (?array $input)
	{
		// It handles the form submission when there is an input.
		if ($input !== null) {
			$email = null;
			$password = null;
	
			if (!empty($input['email']) && !empty($input['password'])) {
				$email = $input['email'];
				$password = $input['password'];
			} else {
				throw new \Exception ('Les données du formulaire sont invalides.');
			}
			$userRepository = new UserRepository();
			$userRepository->connection = new DatabaseConnection;
			$userRepository->loginCheck($email, $password);
		}
		require('templates/users/login.php');
	}
}

