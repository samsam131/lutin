<?php
if (session_status() === 'PHP_SESSION_NONE') {
    session_start();
}
spl_autoload_register(static function (string $fqcn): void {
    $path = sprintf('%s.php', str_replace(['App', '\\'], ['src', '/'], $fqcn));
    require_once $path;
});

require_once('src/controllers/homepage.php');
require_once('src/controllers/commissions/commission.php');
require_once('src/controllers/members/member.php');
require_once('src/controllers/members/members_commission.php');
require_once('src/controllers/members/add_members.php');
require_once('src/controllers/users/login.php');
require_once('src/controllers/meetings/add_meeting.php');
require_once('src/controllers/members/update_member.php');
require_once('src/controllers/meetings/update_meeting.php');
require_once('src/controllers/users/users.php');
require_once('src/controllers/users/add_user.php');
require_once('src/lib/exception.php');

use Application\Controllers\Commission\Commissions;
use Application\Controllers\AddMeeting\AddMeeting;
use Application\Controllers\AddMeeting\AddmeetingForm;
use Application\Controllers\UpdateMeeting\UpdateMeeting;
use Application\Controllers\AddMember\AddMembersForm;
use Application\Controllers\AddMember\Addmembers;
use Application\Controllers\Member\Members;
use Application\Controllers\MemberCommission\MembersCommission;
use Application\Controllers\AddMember\AddCommissionsMember;
use Application\Controllers\UpdateMember\UpdateMember;
use Application\Controllers\AddUser\AddUser;
use Application\Controllers\Login\Login;
use Application\Controllers\User\Users;
use Application\Controllers\Homepage\Homepage;
use Application\Lib\Exception\CommissionIdentifierException;
use Application\Lib\Exception\MeetingIdentifierException;
use Application\Lib\Exception\MemberIdentifierException;

try {

    if (isset($_SESSION['email'])) {

        if (isset($_GET['action']) && $_GET['action'] !== 0) {

            if ($_GET['action'] === 'commission') {
                if (isset($_GET['id']) && $_GET['id'] > 0) {
                    $commission_id = $_GET['id'];
                    (new Commissions())->execute($commission_id);
                } else {
                    throw new Exception('Aucun identifiant de commission na été trouvé');
                    //die;
                }
            } elseif ($_GET['action'] === 'members') {
                (new Members())->execute();

                // Listes des membres d'une commission

            } elseif ($_GET['action'] === 'members_commission') {
                if (isset($_GET['id']) && $_GET['id'] > 0) {
                    $commission_id = $_GET['id'];
                    (new MembersCommission())->execute($commission_id);
                } else {
                    throw new CommissionIdentifierException();
                    //die;
                }
            } elseif ($_GET['action'] === 'update_member') {
                if (isset($_GET['id']) && $_GET['id'] > 0) {
                    $member_id = $_GET['id'];

                    // It sets the input only when the HTTP method is POST (ie. the form is submitted).
                    $input = null;
                    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                        $input = $_POST;
                    }
                    (new UpdateMember())->execute($member_id, $input);
                } else {
                    throw new MemberIdentifierException ();
                    //die;
                }
            } elseif ($_GET['action'] === 'update_meeting') {
                if (isset($_GET['id']) && $_GET['id'] > 0) {
                    $meeting_id = $_GET['id'];

                    // It sets the input only when the HTTP method is POST (ie. the form is submitted).
                    $input = null;
                    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                        $input = $_POST;
                    }
                    (new UpdateMeeting())->execute($meeting_id, $input);
                } else {
                    throw new MeetingIdentifierException();
                    //die;
                }
            } elseif ($_GET['action'] === 'add_meeting') {
                if (isset($_GET['id']) && $_GET['id'] > 0) {
                    $commission_id = $_GET['id'];
                    (new AddMeeting())->execute($commission_id, $_POST);
                } else {
                    throw new Exception ('Aucun identifiant de commission na été trouvé');
                    //die;
                }
            } elseif ($_GET['action'] === 'add_members_form') {
                (new AddMembersForm())->execute();
            } elseif ($_GET['action'] === 'add_members') {
                (new AddMembers())->execute($_POST);
            } elseif ($_GET['action'] === 'add_commissions_member') {
                if (isset($_GET['id']) && $_GET['id'] > 0) {
                    $member_id = $_GET['id'];
                    // It sets the input only when the HTTP method is POST (ie. the form is submitted).
                    $input = null;
                    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                        $input = $_POST['commission'];
                    }
                    (new AddCommissionsMember())->execute($member_id, $input);
                } else {
                    throw new Exception ('Aucun identifiant de membre na été trouvé');
                    //die;
                }
            } elseif ($_GET['action'] === 'home') {
                (new Homepage())->execute();
            } elseif ($_GET['action'] === 'users') { //&& $_SESSION['profile'] === "admin"
                (new Users())->execute();
            } elseif ($_GET['action'] === 'add_user') { //&& $_SESSION['profile'] === "admin"
                // It sets the input only when the HTTP method is POST (ie. the form is submitted).
                $input = null;
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $input = $_POST;
                }
                (new AddUser())->execute($input);
            } elseif ($_GET['action'] === 'add_meeting_form') {
                if (isset($_GET['id']) && $_GET['id'] > 0) {
                    $commission_id = $_GET['id'];
                    (new AddMeetingForm())->execute($commission_id);
                } else {
                    throw new Exception ('Aucun identifiant de commission na été trouvé');
                    //die;
                }
            } else {
                throw new Exception ("Erreur 404 : la page que vous cherchez n'existe pas");
            }
        } else {
            (new Homepage())->execute();
        }
    } else {
        // It sets the input only when the HTTP method is POST (ie. the form is submitted).
        $input = null;
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $input = $_POST;
        }
        (new Login())->execute($input);
    }
} catch (Exception $e) {
    $errorMessage = $e->getMessage();
    require ('templates/error.php');
}
