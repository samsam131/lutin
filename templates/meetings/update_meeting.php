<?php $title = "Réunion"; ?>

<?php ob_start(); ?>


<form action="index.php?action=update_meeting&id=<?= $_GET['id'] ?>" method="POST" enctype="multipart/form-data" class="mx-auto" style="width: 70rem;">
    <!-- Identité du membre -->
    <h1 class="fs-2">Modifier une réunion d'une commission <?= $_GET['id'] ?></h1>
    <div class="row mb-3">
        <div class="col">
            <label for="mmeting_date" class="form-label">Date de la commission</label>
            <input type="date" class="form-control" name="meeting_date" id="meeting_date" value="<?= $meeting->meeting_date ?>" required aria-required="true" min="2023-10-07">
        </div>

        <div class="col">
            <label for="provisional_preparation_date" class="form-label">Date de préparation prévisionnelle</label>
            <input type="date" class="form-control" name="provisional_preparation_date" id="provisional_preparation_date" value=<?= $meeting->ppd ?>>
        </div>

        <div class="col">
            <label for="real_preparation_date" class="form-label">Date réelle de préparation</label>
            <input type="date" class="form-control" name="real_preparation_date" id="real_preparation_date" value=<?= $meeting->rpd ?>>
        </div>

        <div class="col">
            <label for="invitation_date" class="form-label">Date de la convocation</label>
            <input type="date" class="form-control" name="invitation_date" id="invitation_date" value="<?= $meeting->invitation_date ?>" required aria-required="true">
        </div>

    </div>

    <div class="row mb-3">
        <div class="col">
            <label for="quorum" class="form-label">Quorum</label>
            <select name="quorum" id="quorum" class="form-select" aria-label="select-quorum" size="2">
                <option selected value="<?= $meeting->quorum?>"><?= htmlspecialchars($meeting->quorum) ?></option>
                <option value="0">Non atteint</option>
                <option value="1">Atteint</option>
            </select>
        </div>
        <div class="col">

            <label for="agenda" class="form-label">Ordre du jour</label>
            <textarea class="form-control" name="agenda" id="agenda" value="<?= $meeting->agenda ?>"><?= nl2br(htmlspecialchars($meeting->agenda))  ?></textarea>
        </div>
    </div>
    <div class="col">

        <label for="url_minutes" class="form-label">Chemin du fichier</label>
        <input type="text" class="form-control" name="url_minutes" id="url_minutes" value="<?= $meeting->url_minutes ?>"></input>
    </div>

    </div>


    <!-- Organisation -->
    <div class="row mb-3">
        <div class="col">
            <label for="president" class="form-label">Président(e)</label>
            <select name="president" id="president" class="form-select" aria-label="select-president" >
                <option selected value="<?= $meeting->president ?>"><?= htmlspecialchars($meeting->president)  ?></option>
                <option value="Josselin Pibouleau">Josselin Pibouleau</option> <!--A rendre dynamique -->
                <option value="xxx">xxx</option> <!--A rendre dynamique -->
            </select>
        </div>

        <div class="col">
            <label for="assistant" class="form-label">Assistante en charge de la commission</label>
            <select name="assistant" id="assistant" class="form-select col" aria-label="select-assistant" size="2">
                <option selected value="<?= $meeting->assistant ?>"><?= htmlspecialchars($meeting->assistant)  ?></option>
                <option value="Nihad Hina">Nihad Hina</option>
                <option value="Angélique Gagneux">Angélique Gagneux</option>
            </select>
        </div>

        <div class="col">
            <label for="status_minutes" class="form-label">Statut du PV</label>
            <select name="status_minutes" id="status_minutes" class="form-select col" aria-label="select-status_minutes" value="<?= $meeting->status_minutes ?>" required aria-required="true" size="5">
                <option selected value="<?= $meeting->status_minutes ?>"><?= htmlspecialchars($meeting->status_minutes)  ?></option>
                <option value="A_venir">A venir</option>
                <option value="En_cours_de_rédaction">En cours de rédaction</option>
                <option value="En_cours_de_validation">En cours de validation</option>
                <option value="En_cours_d_approbation">En cours d'approbation</option>
                <option value="Approuvé">Approuvé</option>

            </select>
        </div>

    </div>

    <!-- Les commission du membre 
    <div class="container">
        <div class="row">
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_medecin">
                <label class="form-check-label" for="CPL_medecin">CPL_medecin</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_medecin">
                <label class="form-check-label" for="CPL_medecin">CPL_medecin</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPD_infirmier">
                <label class="form-check-label" for="CPD_infirmier">CPD_infirmier</label>
            </div>
        </div>

        <div class="row">
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPLC_taxi">
                <label class="form-check-label" for="CPLC_taxi">CPLC_taxi</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPD_chirurgiens-dentiste">
                <label class="form-check-label" for="CPD_chirurgiens-dentiste">CPD_chirurgiens-dentiste</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPD_orthophoniste">
                <label class="form-check-label" for="CPD_orthophoniste">CPD_orthophoniste</label>
            </div>
        </div>

        <div class="row">
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPR_orthophoniste">
                <label class="form-check-label" for="CPR_orthophoniste">CPR_orthophoniste</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_masseurs-kinésitherapeute">
                <label class="form-check-label" for="CPL_masseurs-kinésitherapeute">CPL_masseurs-kinésitherapeute</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_pharmacien">
                <label class="form-check-label" for="CPL_pharmacien"">CPL_pharmacien" </label>
            </div>
        </div>
        <div class="row">
        <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_CPTS">
                <label class="form-check-label" for="CPL_CPTS">CPL_CPTS</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_transporteurs_sanitaires" disabled>
                <label class="form-check-label" for="CPL_transporteurs_sanitaires">CPL_transporteurs_sanitaires</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_sage_femme" disabled>
                <label class="form-check-label" for="CPL_sage_femme">CPL_sage_femme</label>
            </div>
        </div>
    </div>
    -->

    <button class="btn btn-primary" type="submit">Modifier</button>

</form>

<?php $content = ob_get_clean(); ?>
<?php require('templates/layout.php') ?>