<?php $title = "Membres"; ?>

<?php ob_start(); ?>


<form action="index.php?action=add_members" method="POST" enctype="multipart/form-data" class="mx-auto" style="width: 70rem;">
    <!-- Identité du membre -->
    <h1 class="fs-2">Ajouter un membre</h1>
    <div class="row mb-3">
        <div class="col">
            <label for="first_name" class="form-label">Prénom</label>
            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Entrez le prénom"
            pattern="/^[a-z ,.'-]+$/i" required aria-required="true" minlength="3" maxlength="25">
        </div>

        <div class="col">
            <label for="last_name" class="form-label">Nom</label>
            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Entrez le nom"
            pattern="/^[a-z ,.'-]+$/i" required aria-required="true"  minlength="3" maxlength="25">
        </div>
    </div>

    <div class="row mb-3">
        <div class="col">
            <label for="email" class="form-label">Adresse mail</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Entrez l'adresse mail" required aria-required="true" maxlength="35"
            pattern="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$">
        </div>
        <div class="col">
            <label for="phone" class="form-label">Téléphone</label>
            <input type="tel" class="form-control" name="phone" id="phone" placeholder="0612325678" size="10">
        </div>
    </div>


    <!-- Organisation -->
    <div class="row mb-3">
        <div class="col">
            <label for="section" class="form-label">Setion sociale ou professionnelle</label>
            <select name="section" id="section" class="form-select" aria-label="select-section" required aria-required="true" size="3">
                <option selected></option>
                <option value="sociale">sociale</option>
                <option value="professionnelle">professionnelle</option>
                <option value="autre">autre</option>
            </select>
        </div>

        <div class="col">
        <label for="organisation" class="form-label">Organisation</label>
            <select name="organisation" id="organisation" class="form-select col" aria-label="select-organisation" required aria-required="true" size="7">
                <option selected></option>
                <option value="CPAM">CPAM</option>
                <option value="conseil">Conseil</option>
                <option value="service-medical">Service médical</option>
                <option value="MSA">MSA</option>
                <option value="PS">PS</option>
                <option value="ARS">ARS</option>
                <option value="conseil_ordre">Conseil de l'ordre</option>
            </select>
        </div>

        <div class="col">
        <label for="union_pro" class="form-label">Syndicat</label>
            <select name="union_pro" id="union_pro" class="form-select col" aria-label="select-union" size="7">
                <option selected></option>
                <option value="CFDT">CFDT</option>
                <option value="CGT">CGT</option>
                <option value="FO">FO</option>
                <option value="U2P">U2P</option>
                <option value="UNSA">UNSA</option>
                <option value="mutualite">Mutualité Française</option>
                <option value="unocam">Unocam</option>
            </select>
        </div>

        <div class="row mb-3">
        <div class="col">
            <label for="url_img" class="form-label">Fichier image</label>
            <input type="text" class="form-control" name="url_img" id="url_img">
        </div>
        </div>

    </div>
    
    <!-- Les commission du membre 
    <div class="container my-3">
        <div class="row">
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_medecin" name="CPL_medecin" value="1">
                <label class="form-check-label" for="CPL_medecin">CPL_medecin</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPD_infirmier" value="CPD_infirmier" name="2">
                <label class="form-check-label" for="CPD_infirmier">CPD_infirmier</label>
            </div>
        </div>

        <div class="row">
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPLC_taxi" name="CPLC_taxi" value="4">
                <label class="form-check-label" for="CPLC_taxi">CPLC_taxi</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPD_chirurgiens-dentiste" name="CPD_chirurgiens-dentiste" value="3">
                <label class="form-check-label" for="CPD_chirurgiens-dentiste">CPD_chirurgiens-dentiste</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPD_orthophoniste" name="CPD_orthophoniste" value="6">
                <label class="form-check-label" for="CPD_orthophoniste">CPD_orthophoniste</label>
            </div>
        </div>

        <div class="row">
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPR_orthophoniste" name="CPR_orthophoniste" value="7">
                <label class="form-check-label" for="CPR_orthophoniste">CPR_orthophoniste</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_masseurs-kinésitherapeute" name="CPL_masseurs-kinésitherapeute" value="5">
                <label class="form-check-label" for="CPL_masseurs-kinésitherapeute">CPL_masseurs-kinésitherapeute</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_pharmacien" name="CPL_pharmacien" value="8">
                <label class="form-check-label" for="CPL_pharmacien"">CPL_pharmacien" </label>
            </div>
        </div>
        <div class="row">
        <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_CPTS" name="CPL_CPTS" value="9">
                <label class="form-check-label" for="CPL_CPTS">CPL_CPTS</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_transporteurs_sanitaires" disabled name="CPL_transporteurs_sanitaires" value="10">
                <label class="form-check-label" for="CPL_transporteurs_sanitaires">CPL_transporteurs_sanitaires</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_sage_femme" disabled name="CPL_sage_femme" value="11">
                <label class="form-check-label" for="CPL_sage_femme">CPL_sage_femme</label>
            </div>
        </div>
    </div>
    -->

    <button class="btn btn-primary" type="submit">Envoyer</button>

</form>

<?php $content = ob_get_clean(); ?>
<?php require('templates/layout.php') ?>