<?php $title = "Lutin-membres"; ?>

<?php ob_start(); ?>

<button type="button" class="btn btn-light"><a href="index.php?action=add_members_form">Ajouter un membre</a></button>

<div class="container">
    <div class="row">
        <h1 class="fs-2 mb-3">Liste des membres des commissions</h1>

        

        <?php foreach ($members as $member) { ?>
            <div class="col col-sm-6 col-md-3 mb-3">
                <?php if ($member->organisation === 'PS') { ?>
                    <div class="card text-bg-success col" style="width: 18rem;">
                    <?php } elseif ($member->organisation === 'conseil') { ?>
                        <div class="card text-bg-warning col" style="width: 18rem;">
                        <?php } else { ?>
                            <div class="card text-bg-primary col" style="width: 18rem;">
                            <?php } ?>

                            <div class="card-header fw-bold"><?= htmlspecialchars($member->first_name); ?> <?= $member->last_name; ?></div>

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">email : <?= htmlspecialchars($member->email) ; ?></li>
                                <li class="list-group-item">Téléphone : <?= $member->phone  ?? 'non communiqué'; ?></li>
                                <li class="list-group-item">Section : <?= htmlspecialchars($member->section) ; ?></li>
                                <li class="list-group-item">Organisation : <?= htmlspecialchars($member->organisation) ; ?></li>
                            </ul>
                            <div class="card-footer">
                                <a href="#" class="card-link">Voir ses commissions</a>
                                <a href="mailto:<?= $member->email; ?>" class="card-link">Contacter</a>
                                <a href="index.php?action=update_member&id=<?= $member->member_id; ?>">Modifier</a>
                                <a href="index.php?action=add_commissions_member&id=<?= $member->member_id; ?>">Ajouter des commissions</a>
                            </div>
                            </div>
                        </div>
                    <?php } // The end of the posts loop. 
                    ?>
                    </div>
            </div>




            <?php $content = ob_get_clean(); ?>
            <?php require('templates/layout.php') ?>