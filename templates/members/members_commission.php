<?php $title = "Lutin-membres"; ?>

<?php ob_start(); ?>
<h1>Liste des membres de la commission</h1>

<!--
<?php foreach ($commission_members as $commission_member) { ?>
    <div class="card">
        <div class="card-header">
            <?= htmlspecialchars($commission_member->first_name ?? "Prénom") ?> <?= htmlspecialchars($commission_member->last_name  ?? "Nom") ?>
        </div>
        <div class="card-body">
            <!--
            <h5 class="card-title">email : <?= htmlspecialchars($commission_member->email)  ?? "membre1@email.com"; ?></h5>
            <p class="card-text">Adresse : <?= htmlspecialchars($commission_member->address)  ?? 'non communiquée'; ?></p>
            <p class="card-text">Téléphone : <?= htmlspecialchars($commission_member->phone)  ?? 'non communiqué'; ?></p>
            <p class="card-text">Section : <?= htmlspecialchars($commission_member->section)  ?? "Sociale"; ?></p>
            <p class="card-text">Organisation : <?= htmlspecialchars($commission_member->organisation)  ?? "Conseil"; ?></p>
            
            <a href="#" class="btn btn-primary">Voir les commissions</a>
            <a href="mailto:#>" class="btn btn-primary">contacter</a>
        </div>
    </div> 
<?php  } // The end of the posts loop. ?>
-->

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">Identifiant</th>
            <th scope="col">Nom Prénom</th>
            <th scope="col">Section</th>
            <th scope="col">Organisation</th>
            <th scope="col">Syndicat</th>
            <th scope="col">Adresse mail</th>
            <th scope="col">Téléphone</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($commission_members as $commission_member) { ?>
            <tr>
                <th scope="row"><?= htmlspecialchars($commission_member->member_id) ; ?></th>
                <td><?= htmlspecialchars($commission_member->first_name ?? "Prénom") ?> <?= htmlspecialchars($commission_member->last_name  ?? "Nom") ?></td>
                <td><?= htmlspecialchars($commission_member->section ?? "-"); ?></td>
                <td><?= htmlspecialchars($commission_member->organisation ?? "-"); ?></td>
                <td><?= htmlspecialchars($commission_member->union_pro ?? "-"); ?></td>
                <td><a href="mailto:<?= $commission_member->email; ?>"><?= htmlspecialchars($commission_member->email ?? "membre1@email.com"); ?></a></td>
                <td><?= htmlspecialchars($commission_member->phone ?? '-'); ?></td>
            </tr>
        <?php  } // The end of the posts loop. 
        ?>
    </tbody>
</table>

<?php $content = ob_get_clean(); ?>
<?php require('templates/layout.php') ?>