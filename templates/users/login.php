<?php $title = "Connexion"; ?>

<?php ob_start(); ?>


<form action="index.php?action=login" method="POST" style="width: 50rem;" class="mx-auto">

    <h1 class="fs-2 mb-3">Connexion</h1>
    <div class="row mb-3">
        <div class="my-3">
            <label for="email1" class="form-label">Adresse mail</label>
            <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" required aria-required="require" minlength="7" maxlength="40"
            pattern="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$">
            <div id="emailHelp" class="form-text">Nous ne partageons pas votre adresse avec d'autres organisations.</div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="my-3">
            <label for="Password" class="form-label">Mot de passe</label>
            <input type="password" class="form-control" name="password" id="password" size="8">
        </div>
    </div>


    <button type="submit" class="btn btn-primary">Connexion</button>

</form>

<?php $content = ob_get_clean(); ?>
<?php require('templates/layout.php') ?>