<?php $title = "Connexion"; ?>

<?php ob_start(); ?>


<form action="index.php?action=add_user" method="POST" style="width: 50rem;" class="mx-auto">

    <h1 class="fs-2 mb-3">Ajouter un utilisateur</h1>
    
    <div class="row mb-3">
        <div class="my-3">
            <label for="first_name" class="form-label">Prénom</label>
            <input type="text" class="form-control" name="first_name" id="first_name" required aria-required="true" minlength="3" maxlength="25">
        </div>
    </div>

    <div class="row mb-3">
        <div class="my-3">
            <label for="last_name" class="form-label">Nom</label>
            <input type="text" class="form-control" name="last_name" id="last_name" required aria-required="true" minlength="3" maxlength="25">
        </div>
    </div>
    
    <div class="row mb-3">
        <div class="my-3">
            <label for="email1" class="form-label">Adresse mail</label>
            <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" required aria-required="true" minlength="7" maxlength="40"
            pattern="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$">
           
        </div>
    </div>

    <div class="row mb-3">
        <div class="my-3">
            <label for="Password" class="form-label">Mot de passe</label>
            <input type="password" class="form-control" name="password" id="password" required aria-required="true" size="8">
            <div id="emailHelp" class="form-text">Le mot de passe doit contenir 8 caractères</div>
        </div>
    </div>

    <div class="col">
            <label for="profile" class="form-label">Profil</label>
            <select name="profile" id="profile" class="form-select" aria-label="select-profile" required aria-required="true" size="3">
                <option selected></option>
                <option value="admin">Administrateur</option>
                <option value="modifer">Modificateur</option>
                <option value="consult">Consultant</option>
            </select>
        </div>




    <button type="submit" class="btn btn-primary">Envoyer</button>

</form>

<?php $content = ob_get_clean(); ?>
<?php require('templates/layout.php') ?>