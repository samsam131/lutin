<?php $title = "Lutin-utilisateurs"; ?>

<?php ob_start(); ?>

<button type="button" class="btn btn-light"><a href="index.php?action=add_user">Ajouter un utilisateur</a></button>

<h1>Liste des utilisateurs</h1>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">Identifiant</th>
            <th scope="col">Nom Prénom</th>
            <th scope="col">email</th>
            <th scope="col">Profil</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) { ?>
            <tr>
                <th scope="row"><?= htmlspecialchars($user->user_id) ; ?></th>
                <td><?= htmlspecialchars($user->first_name)  ?? "Prénom" ?> <?= htmlspecialchars($user->last_name)  ?? "Nom" ?></td>
                <td><a href="mailto:<?= $user->email; ?>"><?= htmlspecialchars($user->email)  ?? "membre1@email.com"; ?></a></td>
                <td><?= htmlspecialchars($user->profile)  ?? '-'; ?></td>
            </tr>
        <?php  } // The end of the posts loop. 
        ?>
    </tbody>
</table>

<?php $content = ob_get_clean(); ?>
<?php require('templates/layout.php') ?>