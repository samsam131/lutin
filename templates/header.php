<header>
    <nav class="navbar navbar-expand-md bg-light navbar-light fixed-top">
        <div class="container">
            <a class="navbar-brand text-uppercase fw-bold" href="index.php">
                <img src="images/logo_cpam_indre.png" class="float-start" alt="logo de l'assurance maladie de l'Indre">
                <!-- <span class="bg-primary bg-gradient p-1 rounded-3 text-light">John</span> -->

            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#johndoe">Les commissions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#expertise">Les membres</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#portfolio">Mon compte</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>

        </div>

    </nav>
</header>