<?php
//session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css" />

    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/14273d579a.js" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" integrity="sha384-rOA1PnstxnOBLzCLMcre8ybwbTmemjzdNlILg8O7z1lUkLXozs4DHonlDtnE7fpc" crossorigin="anonymous">
    </script>

    <link rel="stylesheet" href="css/style.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;600&display=swap" rel="stylesheet">
    <link rel="icon" type="image/png" href="images/favicon_io/favicon-32x32.png" />
    <title><?= $title ?></title>
</head>

<header>
    <div class="row bg-white">
        <div class="col-3">
            <a href="http://localhost:8888/lutin/">
                <img src="images/logo_cpam_indre.png" class="float-start" alt="logo de l'assurance maladie de l'Indre">
            </a>

        </div>
        <div class="col-9 text-center p-2">
            <h1>Lutin</h1>
            <h2>Gestion des commissions paritaires des professionnels de santé</h2>
        </div>

    </div>
    
    <nav class="row bg-light py-3 mb-4">
        <div class="col-3 text-center"><a href="index.php?action=members" class="text-reset text-decoration-none">Les membres des commissions</a></div>
        <div class="col-3 border-end border-start border-dark text-center"><a href="index.php" class="text-reset text-decoration-none">Les commissions</a></div>
        <?php if (!empty($_SESSION['email'])) {
            echo '<div class="col-3 text-center"><a href="index.php?action=logout" class="text-reset text-decoration-none">Se déconnecter :' . $_SESSION['profile'] .'</a></div>';
        } else {
            echo '<div class="col-3 text-center"><a href="index.php?action=login" class="text-reset text-decoration-none">Connexion</a></div>';
        } ?>
        <div class="col-3 border-end border-start border-dark text-center"><a href="index.php?action=users" class="text-reset text-decoration-none">Les utilisateurs</a></div>



    </nav>
</header>

<body>
    <?= $content ?>

    <!-- Bootstrap JS bundle -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script>
        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        })
    </script>

    <footer class="bg-light text-center text-lg-start mt-5">
        <div class="row">
            <div class="col-4 text-center p-3">
                © 2021 Copyright : <a class="fw-bold" href="https://ameli.fr/">CPAM de l'Indre</a>
            </div>
            <div class="col-4 text-center p-3">
                <a href="#">Mentions légales</a>
            </div>
            <div class="col-4 text-center p-3">
                <a href="mailto:sami.gafsi@assurance-maladie.fr">Contacter l'administrateur</a>
            </div>
        </div>


    </footer>

</body>


</html>