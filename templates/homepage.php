<?php $title = "Lutin-Accueil"; ?>

<?php ob_start(); ?>

<h1 class="fs-2 mb-3">Liste des commissions PS</h1>

<div class="conatiner">
    <div class="row">
        <?php foreach ($commissions as $commission) { ?>
            <div class="col-12 col-md-3">
                <div class="card shadow-lg bg-body rounded m-1">
                    <img src="<?= $commission->img_url ?>" class="card-img-top" alt="Photo représentant la profession">
                    <div class="card-body">
                        <h5 class="card-title"><?= htmlspecialchars($commission->nickname); ?></h5>
                        <p class="card-text"><?= htmlspecialchars($commission->name); ?></p>
                        <a href="index.php?action=commission&id=<?= htmlspecialchars($commission->commission_id) ?> " class="btn btn-primary my-2">Détail de la commission</a>
                        <a href="index.php?action=members_commission&id=<?= htmlspecialchars($commission->commission_id) ?>" class="btn btn-primary my-2">Liste des membres</a>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>
</div>
<!--
<div>
    <?php
foreach ($commissions as $commission) {
?>
    <div class="news">
        <h3>
            <?= htmlspecialchars($commission->name); ?>
            <em>installée le <?= $commission->installation_date; ?></em>
        </h3>
        <p>
            <?=
            // We display the post content.
            nl2br(htmlspecialchars($commission->nickname));
            ?>
            <br />
            <em><a href="index.php?action=commission&id=<?= urlencode($commission->commission_id) ?> ">Détail des réunions</a></em>
        </p>
    </div>
<?php
} // The end of the posts loop.
?>
</div>
-->
<?php $content = ob_get_clean(); ?>

<?php require('templates/layout.php') ?>