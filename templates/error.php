<?php $title = "Lutin-Erreur"; ?>

<?php ob_start(); ?>

<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading">Erreur</h4>
  <p>Une erreur est survenue : </p>
  <hr>
  <p class="mb-0"><?= $errorMessage ?>.</p>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('templates/layout.php') ?>