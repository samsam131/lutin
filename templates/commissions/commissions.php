<?php $title = "Lutin-Commissions"; ?>

<?php ob_start(); ?>
<h1 class="fs-2 mb-3">Détail de la <?= $commission->nickname; ?></h1>



<div class="container">
    <div class="row">
        <!-- Présentation of commission -->

        <div class="col-12 col-md">
            <h3>Présentation de la commission</h3>
            <div class="card my-3" style="width: 25rem;">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><span class="fw-bold">Installée le :</span> <?= htmlspecialchars($commission->installation_date ?? "-") ; ?></li>
                    <li class="list-group-item"><span class="fw-bold">Nombre de réunion par an : </span><?= htmlspecialchars($commission->frequency ?? "-") ; ?></li>
                    <li class="list-group-item"><span class="fw-bold">Président : </span><?= htmlspecialchars($commission->president) ?? "-" ; ?></li>
                    <li class="list-group-item"><span class="fw-bold">Vice-président : </span><?= htmlspecialchars($commission->vice_president ?? "-") ; ?></li>
                    <li class="list-group-item"><span class="fw-bold">Régles d'alternance de la présidence : </span><?= htmlspecialchars($commission->alternation_rules ?? "-") ; ?></li>
                    <li class="list-group-item"><span class="fw-bold">Composition de la section sociale : </span><?= htmlspecialchars($commission->composition_social_section ?? "-") ; ?></li>
                    <li class="list-group-item"><span class="fw-bold">Composition de la section professionnelle : </span><?= htmlspecialchars($commission->composition_professionnal_section ?? "-") ; ?></li>
                </ul>
                <div class="card-footer"><a href="<?= htmlspecialchars($commission->url_agreement ?? "En cours"); ?>">Consulter la convention et les avenants</a></div>
                <div class="card-footer"><a href="<?= htmlspecialchars($commission->url_invitation_template ?? "En cours"); ?>">Modèle de convocation</a></div>
                <div class="card-footer"><a href="<?= htmlspecialchars($commission->url_minutes_template ?? "En cours"); ?>">Modèle de PV</a></div>
            </div>
        </div>

        <!-- List of meetings -->
        <div class="col-12 col-md">
            <div class="row">        
                    <a href="index.php?action=add_meeting_form&id=<?= htmlspecialchars($commission->commission_id) ?>" class="btn btn-secondary m-3" style="width: 10rem;">Créer une réunion</a>   
            </div>

            <h3>Liste des réunions</h3>
            <?php
            foreach ($meetings as $meeting) {
            ?>
                <div class="card mb-3" style="max-width: 600px;">
                    <div class="row g-0">
                        <div class="col-md-2">
                            <img src="images/CercleBleuAM/CharteAM2021_PictoGenerique_CercleBleuAM_Plan de travail 27.png" class="img-fluid rounded-start" alt="image de calendrier">
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h4 class="card-title">Le <?= htmlspecialchars($meeting->meeting_date ?? ''); ?></h4>
                                <!--<p class="card-text">ODJ <?= htmlspecialchars($meeting->agenda) ; ?></p>-->

                                <p class="card-text">Président :<?= htmlspecialchars($meeting->president ?? "Donnée indisponible"); ?></p>
                                <p class="card-text">Assistante : <?= htmlspecialchars($meeting->assistant ?? "Donnée indisponible"); ?></p>
                                <p class="card-text">Date de préparation prévisionnelle : <?= htmlspecialchars($meeting->ppd ?? "Donnée indisponible"); ?></p>
                                <p class="card-text">Date de préparation réelle : <?= htmlspecialchars($meeting->rpd ?? "Donnée indisponible"); ?></p>
                                <p class="card-text">Date de la convocation : <?= htmlspecialchars($meeting->invitation_date ?? "Donnée indisponible"); ?></p>
                                <p class="card-text">Statut du PV : <?= htmlspecialchars($meeting->status_minutes ?? "Donnée indisponible"); ?></p>
                                <p class="card-text">Quorum :
                                    <?php if ($meeting->quorum === 0) {
                                        echo   "non atteint";
                                    } else {
                                        echo "atteint";
                                    } ?>
                                </p>

                                <?php if ($meeting->status_minutes == "En cours de rédaction" || "En cours de validation") { ?>
                                    <a href="#" class="btn btn-primary" disabled>PV</a> <?php } else { ?>
                                    <a href="#" class="btn btn-primary">PV</a> <?php } ?>

                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#listmb">Participants</button>
                                <a href="templates/edit_invitation.php" class="btn btn-primary">Convocation</a>
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#odj">Ordre du jour</button>
                                <div class="modal fade" id="odj" tabindex="-1" aria-labelledby="odj" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="odj">Ordre du jour</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Texte</p>
                                                <?= htmlspecialchars($meeting->agenda) ; ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <p class="card-footer"><a href="index.php?action=update_meeting&id=<?= htmlspecialchars($meeting->meeting_id) ; ?>">Modifier</a></p>

                            </div>
                        </div>
                    </div>
                </div>
            <?php
            } // The end of the posts loop.
            ?>
        </div>

    </div>
</div>


<?php $content = ob_get_clean(); ?>

<?php require('templates/layout.php') ?>