<?php $title = "Membres"; ?>

<?php ob_start(); ?>


<form action="index.php?action=add_commissions_member&id=<?= $_GET['id']; ?>" method="POST" enctype="multipart/form-data" class="mx-auto" style="width: 70rem;">
    <!-- Identité du membre -->
    <h1 class="fs-2">Ajouter des commissions à un membre</h1>
          
    <!-- Les commission du membre -->
    <div class="container my-3">
        <div class="row">
            <div class="form-check col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_medecin" name="commission[]" value="1">
                <label class="form-check-label" for="CPL_medecin">CPL_medecin</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPD_infirmier" name="commission[]" value="2">
                <label class="form-check-label" for="CPD_infirmier">CPD_infirmier</label>
            </div>
        </div>

        <div class="row">
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPLC_taxi" name="commission[]" value="4">
                <label class="form-check-label" for="CPLC_taxi">CPLC_taxi</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPD_chirurgiens-dentiste" name="commission[]" value="3">
                <label class="form-check-label" for="CPD_chirurgiens-dentiste">CPD_chirurgiens-dentiste</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPD_orthophoniste" name="commission[]" value="6">
                <label class="form-check-label" for="CPD_orthophoniste">CPD_orthophoniste</label>
            </div>
        </div>

        <div class="row">
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPR_orthophoniste" name="commission[]" value="7">
                <label class="form-check-label" for="CPR_orthophoniste">CPR_orthophoniste</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_masseurs-kinésitherapeute" name="commission[]" value="5">
                <label class="form-check-label" for="CPL_masseurs-kinésitherapeute">CPL_masseurs-kinésitherapeute</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_pharmacien" name="commission[]" value="8">
                <label class="form-check-label" for="CPL_pharmacien"">CPL_pharmacien" </label>
            </div>
        </div>
        <div class="row">
        <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_CPTS" name="commission[]" value="9">
                <label class="form-check-label" for="CPL_CPTS">CPL_CPTS</label>
            </div>
            <!--
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_transporteurs_sanitaires" disabled name="commission[]" value="10">
                <label class="form-check-label" for="CPL_transporteurs_sanitaires">CPL_transporteurs_sanitaires</label>
            </div>
            <div class="form-check form-switch col">
                <input class="form-check-input" type="checkbox" role="switch" id="CPL_sage_femme" disabled name="commission[]" value="11">
                <label class="form-check-label" for="CPL_sage_femme">CPL_sage_femme</label>
            </div>
            -->
        </div>
    </div>
    

    <button class="btn btn-primary" type="submit">Envoyer</button>

</form>

<?php $content = ob_get_clean(); ?>
<?php require('templates/layout.php') ?>