<?php
require('../lib/fpdf186/fpdf.php');
 

class PDF extends FPDF
{
// En-t�te
function Header()
{
	//require_once('../src/model.php'); 
    //require_once('../src/controllers/commission.php');
    // Logo
	$this->Image('../images/logo_cpam_indre.png',null,null,0);
	// Police Arial gras 15
	$this->SetFont('Arial','B',15);
	// D�calage � droite
	$this->Cell(80);
	// Titre
	$this->Cell(30,10,'Convocation a la commission paritaire du ',0,0,'C');
	// Saut de ligne
	$this->Ln(20);
}


function CorpsChapitre($fichier)
{
    // Lecture du fichier texte
    $txt = file_get_contents($fichier);
    // Times 12
    $this->SetFont('Times','',12);
    // Sortie du texte justifié
    $this->MultiCell(0,5,$txt);
    // Saut de ligne
    $this->Ln();
    // Mention en italique
    $this->SetFont('','I');
    //$this->Cell(0,5,"(fin de l'extrait)");
}

function TitreChapitre($num, $libelle)
{
    // Arial 12
    $this->SetFont('Arial','',12);
    // Couleur de fond
    //$this->SetFillColor(200,220,255);
    // Titre
    //$this->Cell(0,6,"Chapitre $num : $libelle",0,1,'L',true);
    // Saut de ligne
    $this->Ln(4);
}

function AjouterChapitre($num, $titre, $fichier)
{
    //$this->AddPage();
    $this->TitreChapitre($num,$titre);
    $this->CorpsChapitre($fichier);
}

function AjouterSignature (){
    $this->Image('../images/ignature.jpg',null,null,0);
    $this->Cell(0,6,'Josselin Pibouleau',0,0,'C');
}

// Pied de page
function Footer()
{
	// Positionnement � 1,5 cm du bas
	$this->SetY(-15);
	// Police Arial italique 8
	$this->SetFont('Arial','I',8);
	// Num�ro de page
	$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Instanciation de la classe d�riv�e
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
//$pdf->Cell(0,10,htmlspecialchars('Châteauroux, le 12 juin 2023, Madame, Monsieur, Docteur, Je vous prie d assister à la réunion de la Commission Paritaire Locale des Pharmaciens qui se tiendra le'),0,1);
$pdf->AjouterChapitre(1,'',htmlentities('../lib/fpdf186/invitation.txt'));
//$pdf->AjouterChapitre(2,'LE POUR ET LE CONTRE','20k_c2.txt');
$pdf->Output();
