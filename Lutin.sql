-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : sam. 05 août 2023 à 18:34
-- Version du serveur : 5.7.39
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `Lutin`
--

-- --------------------------------------------------------

--
-- Structure de la table `commissions`
--

CREATE TABLE `commissions` (
  `commission_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `installation_date` date NOT NULL,
  `frequency` int(11) NOT NULL,
  `president` varchar(255) DEFAULT NULL,
  `vice-president` varchar(255) DEFAULT NULL,
  `url_img` varchar(255) DEFAULT NULL COMMENT 'A odifier pour le passage en prod'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commissions`
--

INSERT INTO `commissions` (`commission_id`, `name`, `nickname`, `installation_date`, `frequency`, `president`, `vice-president`, `url_img`) VALUES
(1, 'Commission paritaire locale des médecins libéraux', 'CPL Médecins', '2016-03-02', 4, 'Jean Paul Da Silva', 'Josselin Pibouleau', './images/img_commissions/img_cpl_medecin.jpeg'),
(2, 'Commission paritaire départementale des infirmiers libéraux', 'CPD Infirmiers', '2018-03-15', 3, 'Sonia Gangneux', 'Josselin Pibouleau', './images/img_commissions/img_cpd_infirmier.jpeg'),
(3, 'Commission paritaire départementale des chirurgiens-dentistes', 'CPD chirurgiens-dentistes', '2023-10-19', 1, 'Josselin Pibouleau', 'Dr Bonneville', './images/img_commissions/img_cpd_chir_dentiste.jpg'),
(4, 'Commission paritaire locale de concertation des taxis', 'CPLC Taxi', '2019-03-08', 2, 'M. Jardat', 'Josselin Pibouleau', './images/img_commissions/img_cplc_taxi.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `meeting`
--

CREATE TABLE `meeting` (
  `meeting_id` int(11) NOT NULL,
  `commission_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `quorum` tinyint(1) NOT NULL,
  `agenda` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `meeting`
--

INSERT INTO `meeting` (`meeting_id`, `commission_id`, `date`, `quorum`, `agenda`) VALUES
(1, 1, '2023-03-15', 1, '1. Alternance de la présidence\r\n2. e-santé\r\n3. Démographie'),
(2, 2, '2016-03-17', 0, '1. dépenses\r\n2. e-santé\r\n3. Questions diverses'),
(3, 2, '2023-07-06', 1, '1.Alternance de la présidence\r\n2.Approbation du PV');

-- --------------------------------------------------------

--
-- Structure de la table `members`
--

CREATE TABLE `members` (
  `member_id` int(11) NOT NULL,
  `first_name` varchar(126) NOT NULL,
  `last_name` varchar(126) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` text,
  `phone` int(11) DEFAULT NULL,
  `section` varchar(126) NOT NULL,
  `organisation` varchar(126) NOT NULL,
  `url_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `members`
--

INSERT INTO `members` (`member_id`, `first_name`, `last_name`, `email`, `address`, `phone`, `section`, `organisation`, `url_img`) VALUES
(1, 'Sami', 'Gafsi', 'sami.gafsi@assurance-maladie.fr', '141 avenue de Verdun, 36000 Châteauroux', 632204157, 'sociale', 'CPAM', './images/img_members/sami_gafsi.jpeg'),
(2, 'Thierry', 'Touchet', 'thierry.touchet@gmail.com', NULL, NULL, 'sociale', 'U2P', './images/img_members/default_men.webp'),
(3, 'Jean Paul', 'Da Silva', 'jeanpaul.dasilva@orange.fr', NULL, NULL, 'professionnelle', 'MG France', './images/img_members/dr_dasilva.webp'),
(4, 'Patrice', 'Lamoureux', 'patrice.lamoureux@mail.com', NULL, NULL, 'sociale', 'conseil', './images/img_members/patrice_lamoureux.jpeg'),
(5, 'Sylvaine', 'Le Liboux', 'sylvaine.leliboux@orange.fr', NULL, NULL, 'professionnelle', 'CSMF', './images/img_members/dr_leliboux.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `members_commissions`
--

CREATE TABLE `members_commissions` (
  `member_id` int(11) NOT NULL,
  `commission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `members_commissions`
--

INSERT INTO `members_commissions` (`member_id`, `commission_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`) VALUES
(1, 'Sami', 'Gafsi', 'sami.gafsi@assurance-maladie.fr', '1234'),
(2, 'Nihad', 'Hina', 'nihad.hina@assurance-maladie.fr', '1234'),
(3, 'Angélique', 'Gagneux', 'angelique.gagneux@assurance-maladie.fr', '1234');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commissions`
--
ALTER TABLE `commissions`
  ADD PRIMARY KEY (`commission_id`);

--
-- Index pour la table `meeting`
--
ALTER TABLE `meeting`
  ADD PRIMARY KEY (`meeting_id`),
  ADD KEY `commission_id` (`commission_id`);

--
-- Index pour la table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`member_id`);

--
-- Index pour la table `members_commissions`
--
ALTER TABLE `members_commissions`
  ADD PRIMARY KEY (`member_id`,`commission_id`),
  ADD KEY `commission_id` (`commission_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commissions`
--
ALTER TABLE `commissions`
  MODIFY `commission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `meeting`
--
ALTER TABLE `meeting`
  MODIFY `meeting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `members`
--
ALTER TABLE `members`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `meeting`
--
ALTER TABLE `meeting`
  ADD CONSTRAINT `meeting_ibfk_1` FOREIGN KEY (`commission_id`) REFERENCES `commissions` (`commission_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `members_commissions`
--
ALTER TABLE `members_commissions`
  ADD CONSTRAINT `members_commissions_ibfk_2` FOREIGN KEY (`commission_id`) REFERENCES `commissions` (`commission_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `members_commissions_ibfk_3` FOREIGN KEY (`member_id`) REFERENCES `members` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
